package com.demo.tiercity.domain.use_case

import com.demo.tiercity.common.Constants.unreachable_network
import com.demo.tiercity.common.Resource
import com.demo.tiercity.common.Utils.Companion.getErrorReason
import com.demo.tiercity.data.remote.dto.VehiclesDto
import com.demo.tiercity.data.repository.BaseRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetVehiclesUseCase @Inject constructor(
    private val repository: BaseRepository
) {

    operator fun invoke(uuid1: String, uuid2: String, apikey: String): Flow<Resource<VehiclesDto>> = flow {
        try {
            emit(Resource.Loading<VehiclesDto>())
            val authResponse = repository.getVehicles(uuid1, uuid2, apikey)
            emit(Resource.Success<VehiclesDto>(authResponse))
        } catch(e: HttpException) {
            emit(Resource.Error<VehiclesDto>(getErrorReason(e)))
        } catch(e: IOException) {
            emit(Resource.Error<VehiclesDto>(unreachable_network.toString()))
        }
    }
}