package com.demo.tiercity.domain.model

enum class VehType {
    escooter,
    ebicycle,
    emoped
}