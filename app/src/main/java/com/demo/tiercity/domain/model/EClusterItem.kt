package com.demo.tiercity.domain.model

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

class EClusterItem(
    id: String,
    lat: Double,
    lng: Double,
    type: VehType,
    batteryLevel: Int
) : ClusterItem {

    private val position: LatLng
    private val type: VehType
    private val batteryLevel: Int

    override fun getPosition(): LatLng {
        return position
    }

    override fun getTitle(): String {
        return ""
    }

    override fun getSnippet(): String {
        return ""
    }

    fun getBatteryLevel(): Int {
        return batteryLevel
    }

    fun getType(): VehType {
        return type
    }

    init {
        position = LatLng(lat, lng)
        this.type = type
        this.batteryLevel = batteryLevel
    }
}