package com.demo.tiercity.presentation.vehicles_map

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.demo.tiercity.common.Resource
import com.demo.tiercity.domain.use_case.GetVehiclesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class VMapViewModel @Inject constructor(
    private val getItemsUseCase: GetVehiclesUseCase
): ViewModel() {

    private val _state = MutableLiveData<VehState>()
    val state: LiveData<VehState> = _state

    fun getVehicles(uuid1: String, uuid2: String, apiKey: String) {
        getItemsUseCase(uuid1, uuid2, apiKey).onEach { result ->
            when (result) {
                is Resource.Success -> {
                    _state.value = VehState(authResponse = result.data)
                }
                is Resource.Error -> {
                    _state.value = VehState(error = result.message)
                }
                is Resource.Loading -> {
                    _state.value = VehState(isLoading = true)
                }
            }
        }.launchIn(viewModelScope)
    }

}