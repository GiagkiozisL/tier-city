package com.demo.tiercity.presentation.vehicles_map

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.demo.tiercity.R
import com.demo.tiercity.common.Constants.apiKey
import com.demo.tiercity.common.Constants.userLat
import com.demo.tiercity.common.Constants.userLng
import com.demo.tiercity.common.Constants.uuid1
import com.demo.tiercity.common.Constants.uuid2
import com.demo.tiercity.common.Utils.Companion.getErrorFromCode
import com.demo.tiercity.common.toClusterItem
import com.demo.tiercity.data.remote.dto.Vehicle
import com.demo.tiercity.databinding.ActivityMapsBinding
import com.demo.tiercity.domain.model.EClusterItem
import com.demo.tiercity.domain.model.VehType
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.snackbar.Snackbar
import com.google.maps.android.clustering.Cluster
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.ClusterManager.OnClusterClickListener
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import com.google.maps.android.ui.IconGenerator
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class VMapActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var map: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    private lateinit var mapFragment: SupportMapFragment
    private lateinit var layout: View
    private lateinit var berlin: LatLng
    private val viewModel by viewModels<VMapViewModel>()
    private lateinit var clusterManager: ClusterManager<EClusterItem>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        layout = binding.mainLayout
        setContentView(binding.root)

        supportActionBar?.elevation = 0F

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment

        checkLocationPermission()
        addObserver()
    }

    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                mapFragment.getMapAsync(this)
            }
        }

    private fun checkLocationPermission() {
        when {
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED -> {
                mapFragment.getMapAsync(this)
                binding.bgImageView.visibility = View.GONE
            }

            ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) -> {
                layout.showSnackBar(
                    binding.mainLayout,
                    getString(R.string.permission_required),
                    Snackbar.LENGTH_INDEFINITE,
                    getString(R.string.ok)
                ) {
                    requestPermissionLauncher.launch(
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                }
            }

            else -> {
                requestPermissionLauncher.launch(
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            }
        }
    }

    private fun addObserver() {
        viewModel.state.observe(this@VMapActivity, Observer { state ->
            binding.progressBar.visibility = if (state.isLoading) View.VISIBLE else View.GONE
            if (state.error.isNotEmpty()) {
                val errorMsg = getErrorFromCode(this, state.error)
                layout.showSnackBar(
                    binding.mainLayout,
                    errorMsg,
                    Snackbar.LENGTH_INDEFINITE,
                    "")
                {
                    // i could retry here :)
                }
            }

            state.authResponse?.let {
                println("total vehicles ${it.vehicles}")
                placeMarkersInMap(it.vehicles)
            }
        })
    }

    private fun View.showSnackBar(
        view: View,
        msg: String,
        length: Int,
        actionMessage: CharSequence?,
        action: (View) -> Unit
    ) {
        val snackBar = Snackbar.make(view, msg, length)
        if (actionMessage != null) {
            snackBar.setAction(actionMessage) {
                action(this)
            }.show()
        } else {
            snackBar.show()
        }
    }

    private fun placeMarkersInMap(vehicles: List<Vehicle>) {
        for (vehicle in vehicles) {
            clusterManager.addItem(vehicle.toClusterItem())
        }

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(berlin, 12F))
    }

    private fun showBottomSheet(clusterItem: EClusterItem) {
        val vehicleFragment = VehicleDetailsBSheet(clusterItem)
        vehicleFragment.show(supportFragmentManager, vehicleFragment::class.java.toString())
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just somewhere between servers location data coordinates.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        // Add a marker and move the camera
        berlin = LatLng(userLat, userLng)
        val bmDesc = BitmapDescriptorFactory.fromResource(R.drawable.red_marker)

        // Initialize the manager with the context and the map.
        // (Activity extends context, so we can pass 'this' in the constructor.)
        clusterManager = ClusterManager(this, map)
        clusterManager.setAnimation(false)
        clusterManager.renderer = TClusterRenderer(this, googleMap, clusterManager)
        clusterManager.setOnClusterClickListener {
            false
        }

        clusterManager.setOnClusterItemClickListener {
            println("show bottom sheet")
            showBottomSheet(it)
            false
        }

        with (map) {
            addMarker(MarkerOptions()
                .position(berlin)
                .icon(bmDesc))
            setOnCameraIdleListener(clusterManager)
            setOnMarkerClickListener(clusterManager)
            moveCamera(CameraUpdateFactory.newLatLngZoom(berlin, 10F))
        }

        // fetch data
        viewModel.getVehicles(uuid1, uuid2, apiKey)
    }

    inner class TClusterRenderer(
        context: Context,
        map: GoogleMap?,
        clusterManager: ClusterManager<EClusterItem>?
    ) : DefaultClusterRenderer<EClusterItem>(context, map, clusterManager) {

        private var iconGenerator: IconGenerator? = null
        private var defaultBitmap: BitmapDescriptor
        private var scooterBitmap: BitmapDescriptor
        private var bicycleBitmap: BitmapDescriptor
        private var mopedBitmap: BitmapDescriptor

        init {
            iconGenerator = IconGenerator(context)
            defaultBitmap = BitmapDescriptorFactory.fromResource(R.drawable.red_marker)
            scooterBitmap = BitmapDescriptorFactory.fromResource(R.drawable.marker_scooter_default)
            bicycleBitmap = BitmapDescriptorFactory.fromResource(R.drawable.marker_ebike_default)
            mopedBitmap = BitmapDescriptorFactory.fromResource(R.drawable.marker_moped_default)
        }

        override fun onBeforeClusterItemRendered(item: EClusterItem, markerOptions: MarkerOptions) {
            super.onBeforeClusterItemRendered(item, markerOptions)
            markerOptions.icon(chooseBitmapByType(item.getType()))
        }

        override fun getMarker(clusterItem: EClusterItem?): Marker {
            return super.getMarker(clusterItem)
        }

        private fun chooseBitmapByType(type: VehType): BitmapDescriptor {
            return when (type) {
                VehType.escooter -> scooterBitmap
                VehType.ebicycle -> bicycleBitmap
                VehType.emoped -> mopedBitmap
            }
        }
    }
}
