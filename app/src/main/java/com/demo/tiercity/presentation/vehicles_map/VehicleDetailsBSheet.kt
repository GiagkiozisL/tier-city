package com.demo.tiercity.presentation.vehicles_map

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.demo.tiercity.R
import com.demo.tiercity.common.Constants.userLat
import com.demo.tiercity.common.Constants.userLng
import com.demo.tiercity.common.Utils.Companion.calculateDistance
import com.demo.tiercity.common.Utils.Companion.distanceText
import com.demo.tiercity.databinding.FragmentVehicleDetailsBinding
import com.demo.tiercity.domain.model.EClusterItem
import com.demo.tiercity.domain.model.VehType
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class VehicleDetailsBSheet(
    private val eClusterItem: EClusterItem
): BottomSheetDialogFragment() {

    private lateinit var binding: FragmentVehicleDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = View.inflate(context, R.layout.fragment_vehicle_details, null)
        binding = FragmentVehicleDetailsBinding.bind(view)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val userLocation = LatLng(userLat, userLng)
        val vehicleLocation = eClusterItem.position
        val distanceMeters = calculateDistance(userLocation, vehicleLocation)
        val distanceKmStr = distanceText(distanceMeters)

        binding.vehicleBatteryTxt.text = String.format(getString(R.string.battery_level), eClusterItem.getBatteryLevel())
        binding.vehicleDistanceTxt.text = String.format(getString(R.string.distance_away, distanceKmStr))
        binding.vehicleTypeImg.setImageResource(chooseBitmapByType(eClusterItem.getType()))
    }

    private fun chooseBitmapByType(type: VehType): Int {
        return when (type) {
            VehType.escooter -> R.drawable.marker_scooter_selected
            VehType.ebicycle -> R.drawable.marker_ebike_selected
            VehType.emoped -> R.drawable.marker_moped_selected
        }
    }
}