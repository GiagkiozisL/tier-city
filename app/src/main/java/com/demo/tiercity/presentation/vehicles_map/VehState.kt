package com.demo.tiercity.presentation.vehicles_map

import com.demo.tiercity.data.remote.dto.VehiclesDto

data class VehState(
    val isLoading: Boolean = false,
    val authResponse: VehiclesDto? = null,
    val error: String = "")
{
}