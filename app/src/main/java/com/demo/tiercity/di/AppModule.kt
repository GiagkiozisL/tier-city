package com.demo.tiercity.di

import android.content.Context
import com.demo.tiercity.common.Constants
import com.demo.tiercity.data.remote.TApi
import com.demo.tiercity.data.repository.BaseRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideTApi(@ApplicationContext context: Context): TApi {
        return Retrofit.Builder()
            .baseUrl(Constants.base_url)
            .client(provideOkHttpClient(context))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(TApi::class.java)
    }

    @Provides
    @Singleton
    fun provideMSRepository(api: TApi): BaseRepository {
        return BaseRepository(api)
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(@ApplicationContext context: Context): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        return OkHttpClient.Builder().build()
    }
}