package com.demo.tiercity

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CityApp: Application() {
}