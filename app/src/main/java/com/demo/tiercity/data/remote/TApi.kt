package com.demo.tiercity.data.remote

import com.demo.tiercity.data.remote.dto.VehiclesDto
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TApi {

    @GET("/v1/json/{id1}/{id2}")
    suspend fun getVehicles(@Path(value = "id1", encoded = true) uuid1: String,
                            @Path(value = "id2", encoded = true) uuid2: String,
                            @Query("apiKey") apiKey: String
    ): VehiclesDto

}