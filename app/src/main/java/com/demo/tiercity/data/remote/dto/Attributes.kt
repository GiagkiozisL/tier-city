package com.demo.tiercity.data.remote.dto


import com.demo.tiercity.domain.model.VehType
import com.google.gson.annotations.SerializedName

data class Attributes(
    @SerializedName("batteryLevel")
    val batteryLevel: Int,
    @SerializedName("hasHelmetBox")
    val hasHelmetBox: Boolean,
    @SerializedName("lat")
    val lat: Double,
    @SerializedName("lng")
    val lng: Double,
    @SerializedName("maxSpeed")
    val maxSpeed: Int,
    @SerializedName("vehicleType")
    val vehicleType: VehType
)