package com.demo.tiercity.data.repository

import com.demo.tiercity.data.remote.TApi
import com.demo.tiercity.data.remote.dto.VehiclesDto
import javax.inject.Inject

class BaseRepository @Inject constructor(
    private val api: TApi
) {

    suspend fun getVehicles(uuid1: String, uuid2: String, apiKey: String) : VehiclesDto {
        return api.getVehicles(uuid1, uuid2, apiKey)
    }
}