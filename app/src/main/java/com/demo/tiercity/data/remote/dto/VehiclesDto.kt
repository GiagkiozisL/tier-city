package com.demo.tiercity.data.remote.dto


import com.google.gson.annotations.SerializedName

data class VehiclesDto(
    @SerializedName("data")
    val vehicles: List<Vehicle>
)