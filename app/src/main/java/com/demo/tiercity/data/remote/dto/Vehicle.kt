package com.demo.tiercity.data.remote.dto


import com.google.gson.annotations.SerializedName

data class Vehicle(
    @SerializedName("attributes")
    val attributes: Attributes,
    @SerializedName("id")
    val id: String,
    @SerializedName("type")
    val type: String
) {

}