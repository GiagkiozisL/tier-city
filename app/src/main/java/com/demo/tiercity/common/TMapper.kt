package com.demo.tiercity.common

import com.demo.tiercity.data.remote.dto.Vehicle
import com.demo.tiercity.domain.model.EClusterItem

fun Vehicle.toClusterItem() : EClusterItem {
    return EClusterItem(
        id = id,
        lat = attributes.lat,
        lng = attributes.lng,
        type = attributes.vehicleType,
        batteryLevel = attributes.batteryLevel)
}