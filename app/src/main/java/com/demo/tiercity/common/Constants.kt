package com.demo.tiercity.common

object Constants {
    const val base_url = "https://api.jsonstorage.net"
    const val userLat = 52.520
    const val userLng = 13.4050

    // errors
    const val unexpected_error = 10001 //  "An unexpected error occurred"
    const val unreachable_network = 10002 // Couldn't reach server. Check your internet connection.

    // vals
    const val uuid1 = "9ec3a017-1c9d-47aa-8c38-ead2bfa9b339"
    const val uuid2 = "c284fd9a-c94e-4bfa-8f26-3a04ddf15b47"
    const val apiKey = "9ef7d5b3-21c7-4a78-a92b-91efef42cabb"
}