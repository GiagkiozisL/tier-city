package com.demo.tiercity.common

import android.content.Context
import android.location.Location
import com.demo.tiercity.R
import com.demo.tiercity.common.Constants.unexpected_error
import com.demo.tiercity.common.Constants.unreachable_network
import com.google.android.gms.maps.model.LatLng
import org.json.JSONObject
import retrofit2.HttpException
import java.util.*
import kotlin.math.roundToInt

class Utils {

    companion object {
        fun getErrorReason(e: HttpException): String {
            // handle 404, 500
            val reason = "${e.code()} - ${e.message()}"
            val defMsg = e.localizedMessage ?: unexpected_error.toString()
            return if (reason.isNullOrEmpty()) defMsg else reason
        }

        fun getErrorFromCode(ctx: Context, errorMsg: String): String {
            if (isNumeric(errorMsg)) {
                return errorByCode(ctx, errorMsg.toInt())
            }
            return errorMsg
        }

        private fun isNumeric(toCheck: String): Boolean {
            return toCheck.toIntOrNull() != null
        }

        private fun errorByCode(ctx: Context, code: Int): String {
            when (code) {
                unexpected_error -> return ctx.getString(R.string.unexpected_error)
                unreachable_network -> return ctx.getString(R.string.unreachable_network)
            }
            return ctx.getString(R.string.unexpected_error)
        }

        fun calculateDistance(locationA: LatLng, locationB: LatLng): Float {
            val results = FloatArray(1)
            Location.distanceBetween(locationA.latitude, locationA.longitude, locationB.latitude, locationB.longitude, results)
            // distance in meter
            return results[0]
        }

        fun distanceText(distance: Float): String {
            val distanceString: String

            if (distance < 1000)
                if (distance < 1)
                    distanceString = String.format(Locale.US, "%dm", 1)
                else
                    distanceString = String.format(Locale.US, "%dm", distance.roundToInt())
            else if (distance > 10000)
                if (distance < 1000000)
                    distanceString = String.format(Locale.US, "%dkm", (distance / 1000).roundToInt())
                else
                    distanceString = "FAR"
            else
                distanceString = String.format(Locale.US, "%.2fkm", distance / 1000)

            return distanceString
        }
    }
}